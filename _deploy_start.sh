
SECONDS=0
source config.sh
die () {
	echo >&2 "$@"
	exit 1
}

[ $1 ] || die "site version required as first argument"
[ $2 ] || die "branch required as second argument"


VERSION=$1
BRANCH=$2

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
# define git repository
repository="git@vps:$GITNAME"


# define git folder
localFolder=$DIR"/repo/"
srcFolder="$DIR/$VERSION/"
profilerDir="$DIR/profiler/$VERSION/"
DATETIME=$(date +%F-%H-%M-%S)



# check src folder
[ -d $srcFolder ] || die "Directory $srcFolder does not exist"

# clone git if not exist
if [ ! -d $localFolder ]; then
	git clone "$repository" $localFolder
fi
# to git folder
cd $localFolder

# read current branch
CB=$(git rev-parse --abbrev-ref HEAD)

echo " current branch: $CB"

exists=$(git rev-parse --verify "$BRANCH")

echo " current branch exists: $exists"

# switch to required branch
if [ "$CB" != "$BRANCH" ] && [ -z "$exists" ]
then    
	echo " checkout remote branch: $BRANCH"
	git fetch
	git checkout -b "$BRANCH" "remotes/origin/$BRANCH" --	
fi
if [ "$CB" != "$BRANCH" ] && [ -n "$exists" ]
then    
	echo " checkout locale branch: $BRANCH"
	git checkout "$BRANCH"
fi


CB=$(git rev-parse --abbrev-ref HEAD)
if [ "$CB" != "$BRANCH" ]
then    
	die " branch $BRANCH not valid"
fi

echo " git pull: $BRANCH"

git pull

rsync -a --delete --exclude '.git/' --exclude 'vendor/' --exclude 'var/' --exclude 'app/config/parameters.yml' $localFolder "$srcFolder/webroot/" --exclude '.bowerrc' --exclude 'web'


cd $srcFolder"/webroot/"
