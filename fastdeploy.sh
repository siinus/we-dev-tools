#/bin/bash
echo -e "\e[00;31mwe fast deploy script\e[00m"
source config.sh
source _deploy_start.sh

rm -rf var/cache/*
make classifier
make webr

# when container from command line is created then log or/and deprecated log was not shown
rm -f var/cache/dev/appDevDebugProject*
source _deploy_end.sh