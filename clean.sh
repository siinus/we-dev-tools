SECONDS=0

echo -e "\e[00;31m Clean log & profiler \e[00m"
die () {
	echo >&2 "$@"
	exit 1
}


for i in {1..10}
do
	if [ -d "dev$i" ]; then
		continue 
	fi
	echo "truncate $i"
	for logfile in "dev$i/logs/access" "dev$i/logs/error"
    do		
		echo "truncated apache log $logfile"
		cat /dev/null > $logfile
	done
	echo "truncate dev$i profiler"
	rm -rf dev$i/webroot/var/profiler/*
	
	echo "truncate dev$i symfony logs"
	rm -f dev$i/webroot/var/logs/*.log
done

for logfile in "test/logs/access" "test/logs/error"
do		
		echo "truncated apache log $logfile"
		cat /dev/null > $logfile
done

echo "truncate test symfony logs"
rm -f test/webroot/var/logs/*.log
find test/webroot/var/profiler/* -mtime +30 -exec rm -r {} \;



testsecs=$(date +%s -d "30 days ago")

 ( while IFS= read -r line; do
  read -r x d <<< "$line" 
  IFS=',' read -r -a array <<< "$line"
  INDEX=$(( ${#array[@]} - 3 ));

  #echo ${array[$INDEX]};
  if ((( "${array[$INDEX]}") > $testsecs )); then
	echo $line
  fi
done < test/webroot/var/profiler/index.csv) > index_tmp.csv
mv index_tmp.csv test/webroot/var/profiler/index.csv

